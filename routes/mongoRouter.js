var express = require('express');
var router = express.Router();
var path = require('path');
var app=express()
const connectMongo = require("../connector/connectMongo.js");
connectMongo()
const People = require("../config/peopleSchema.js");

// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'ejs');

/* GET home page. */
// router.get('/', function (req, res, next) {
//   res.render('index', { title: 'Express' });
// });

router.get("/", (req, res) => {
  res.render("index",{req})
})

router.post("/login", async (req, res) => {
  const { email, password } = req.body;
  try {
    const person = await People.findOne({ email: email })
    // console.log(password)
    // console.log(person.password)
    if (person.password == password) {
      // res.render("detail", { p })
      res.redirect("/mongo/show")
    }
    else {
      res.render("error")
    }
  }
  catch (e) {
    res.render("error")
  }
})

router.get("/register", async (req, res) => {
  res.render("register",{req})
})
router.post("/register", async (req, res) => {
  const { firstName, lastName, email, password, gender } = req.body
  const person = new People({ firstName: firstName, lastName: lastName, email: email, password: password, gender: gender });
  await person.save();
  res.redirect("/mongo/show")
})
router.get("/show", async (req, res) => {
  const db_persons = await People.find({})
  console.log(req.url)
  res.render("show", { req,db_persons })
})

router.get("/delete/:id", async (req, res) => {
  const { id } = req.params
  await People.deleteOne({ _id: id })
  res.redirect("/mongo/show")
})

const gender = ["male", "female"]

router.get("/update/:id", async (req, res) => {
  try {
    const { id } = req.params
    const db_person = await People.findById(id)
    res.render("edit", { db_person, gender,req })
  }
  catch (err) {
    console.log(err)
  }
})

router.put("/update/:id", async (req, res) => {
  const { id } = req.params
  const { firstName, lastName, email, password, gender } = req.body
  const person = await People.findByIdAndUpdate(id, { $set: { firstName: firstName, lastName: lastName, email: email, password: password, gender: gender } }, { runValidators: true, new: true })
  // res.render("detail", { p }) 


  res.redirect("/mongo/show")
})


module.exports = router;
