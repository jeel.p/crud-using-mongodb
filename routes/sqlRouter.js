const express = require('express');
const router = express.Router();
const db = require("../models")
const app=express();
const path = require('path');

// router.get("/api", async (req, res) => {
//     const todos = await db.Todo.findAll()
//     // .then(todos => res.send(todos));
//     res.send(todos);
// })


// app.set('views', path.join(__dirname, 'views2'));
// app.set('view engine', 'ejs');


router.get("/", (req, res) => {
    res.render("index",{req})
})

router.post("/login", async (req, res) => {
    const { email, password } = req.body;
    try {
        // console.log(email+"   "+ password)
        const db_person = await db.Person.findOne({
            where: {
                email: email
            }
        })
        // console.log(password)
        // console.log(person.password)
        if (db_person.password == password) {
            // res.render("detail", { p })
            res.redirect("/sql/show")
        }
        else {
            res.render("error")
        }
    }
    catch (e) {
        res.render("error")
    }
})

router.get("/register", async (req, res) => {
    res.render("register",{req})
})
router.post("/register", async (req, res) => {
    const { firstName, lastName, email, password, gender } = req.body
    const db_person = db.Person.create({ 
        firstName: firstName, lastName: lastName, email: email, password: password, gender: gender 
    });
    // await person.save();
    res.redirect("/sql/show")
})
router.get("/show", async (req, res) => {
    const db_persons = await db.Person.findAll()
    // console.log(db_persons)
    res.render("show", { db_persons,req })
})

router.get("/delete/:id", async (req, res) => {
    const { id } = req.params
    await db.Person.destroy({
        where: {id: id}
    })
    res.redirect("/sql/show")
})

const gender = ["male", "female"]

router.get("/update/:id", async (req, res) => {
    try {
        const { id } = req.params
        console.log(id)
        const db_person = await db.Person.findOne({
            where: {id: id}
        })
        console.log("db_person: ",db_person)
        res.render("edit", { db_person, gender,req })
    }
    catch (err) {
        console.log(err)
    }
})

router.put("/update/:id", async (req, res) => {
    const { id } = req.params
    const { firstName, lastName, email, password, gender } = req.body
    const db_person = await db.Person.update({
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: password,
        gender: gender
    },
        {
            where: { id: id }
        })
    // res.render("detail", { p }) 

    res.redirect("/sql/show")
})
// router.get("*",(req,res)=>{
//     res.render("error")
// })


module.exports = router;