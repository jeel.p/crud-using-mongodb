// const { DataTypes } = require("sequelize/types");

module.exports=(sequelize,DataTypes)=>{
    const Person=sequelize.define("Person",{
        firstName:{
            type:DataTypes.STRING,
            allowNull:false
        },
        lastName:{
            type:DataTypes.STRING,
            allowNull:false
        },
        email:{
            type:DataTypes.STRING,
            allowNull:false
        },
        password:{
            type:DataTypes.STRING,
            allowNull:false
        },
        gender:{
            type:DataTypes.STRING,
            allowNull:false
        }
    })
    return Person
}