const mongoose = require('mongoose');
const connectMongo=()=>{
    mongoose.connect('mongodb://localhost:27017/people')
    .then(() => {
        console.log("Connected successfully")
    })
    .catch((e) => {
        console.log("Something went wrong")
        console.log(e)
    })
}
module.exports=connectMongo
